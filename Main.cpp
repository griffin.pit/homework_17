#include <iostream>
#include <Math.h>

class Vector
{

private:
	double x, y, z;

public:
	Vector(): x(0), y(0), z(0)
	{}
	Vector(double _x, double _y, double _z): x(_x), y(_y), z(_z)
	{}

	double VecLength()
	{
		double VLength = sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2));
		
		return VLength;
		

	}
	
	double GetX()
	{
		return x;
	}

	double GetY()
	{
		return y;
	}

	double GetZ()
	{
		return z;
	}
	
	void SetX(double _x)
	{
		x = _x;
	}

	void SetY(double _y)
	{
		x = _y;
	}

	void SetZ(double _z)
	{
		x = _z;
	}


		
};





int main()
{
	Vector Vec(1,2,2);
	std::cout << "length of vector (" << Vec.GetX() << ';' << Vec.GetY() << ';' << Vec.GetZ() << ") = " << Vec.VecLength();




}